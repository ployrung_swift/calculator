//
//  ContentView.swift
//  Calculator_61160154
//
//  Created by informatics on 2/25/21.
//

import SwiftUI

struct ContentView: View {
    func checkSymbols(symbol:String) {
        self.check = false
        if (symbol == "+") {
            self.symbol = "+"
        }else if (symbol == "-") {
            self.symbol = "-"
        }else if (symbol == "x") {
            self.symbol = "x"
        }else if (symbol == "/") {
            self.symbol = "/"
        }
    }
    
    func inputString(input:String) {
        if(check == true){
            self.input1 = input1+input
        }else{
            self.input2 = input2+input
        }
    }
    func outputString() {
        self.dou1 = Double(self.input1) ?? 0
        self.dou2 = Double(self.input2) ?? 0
        self.equal = "="
        if(symbol == "+"){
            self.output = dou1+dou2
            self.sceneOutput = String(format: "%.0f",output)
        }else if (symbol == "-") {
            self.output = dou1-dou2
            self.sceneOutput = String(format: "%.0f",output)
        }else if (symbol == "x") {
            self.output = dou1*dou2
            self.sceneOutput = String(format: "%.0f",output)
        }else if (symbol == "/") {
            self.output = dou1/dou2
            self.sceneOutput = String(format: "%.0f",output)
        }
        
    }
    func clearValue() {
        self.input1 = String()
        self.input2 = String()
        self.symbol = ""
        self.output = Double()
        self.dou1 = Double()
        self.dou2 = Double()
        self.check = true
        self.sceneOutput = ""
        self.equal = ""
    }
    @State var input1 = String()
    @State var input2 = String()
    @State var symbol = ""
    @State var output = Double()
    @State var sceneOutput = ""
    @State var equal = ""
    
    @State var dou1 = Double()
    @State var dou2 = Double()
    @State var check = true
    var body: some View {
        
        VStack{
            Text("C A L C U L A T O R")
                .padding()
                .font(.largeTitle)
            HStack{
                Text("\(input1) \(symbol) \(input2) \(equal) \(sceneOutput)")
                    .fontWeight(.semibold)
                    .multilineTextAlignment(.center)
                    .frame(width: 400, height: 100)
                    .font(.largeTitle)
                    .border(Color.black,width: 2)
                    .padding()
            }
//            HStack{
//                Text("\(input2)")
//                    .frame(width: 300, height: 50)
//                    .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/,width: 2)
//                    .padding()
//            }
//            HStack{
//                Text("\(sceneOutput)")
//                    .frame(width: 300, height: 50)
//                    .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/,width: 2)
//                    .padding()
//            }
            
            
            
            
            HStack{
                Button(action: {inputString(input: "1")}, label: {
                    Text("1")
                        .fontWeight(.heavy)
                        .padding()
                        .frame(width: 80)
                        .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                })
                Button(action: {inputString(input: "2")}, label: {
                    Text("2")
                        .fontWeight(.heavy)
                        .padding()
                        .frame(width: 80)
                        .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                })
                Button(action: {inputString(input: "3")}, label: {
                    Text("3")
                        .fontWeight(.heavy)
                        .padding()
                        .frame(width: 80)
                        .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                })
                Button(action: {checkSymbols(symbol: "+")}, label:{
                    Text("+")
                        .fontWeight(.heavy)
                        .padding()
                        .frame(width: 100)
                        .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                        .foregroundColor(Color.black)
                })
                .background(ButtonBackground())
            }
            .padding()
            HStack{
                Button(action: {inputString(input: "4")}, label: {
                    Text("4")
                        .fontWeight(.heavy)
                        .padding()
                        .frame(width: 80)
                        .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                })
                Button(action: {inputString(input: "5")}, label: {
                    Text("5")
                        .fontWeight(.heavy)
                        .padding()
                        .frame(width: 80)
                        .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                })
                Button(action: {inputString(input: "6")}, label: {
                    Text("6")
                        .fontWeight(.heavy)
                        .padding()
                        .frame(width: 80)
                        .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                })
                Button(action: {checkSymbols(symbol: "-")}, label: {
                    Text("-")
                        .fontWeight(.heavy)
                        .padding()
                        .frame(width: 100)
                        .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                        .foregroundColor(Color.black)
                })
                .background(ButtonBackground())
            }
            .padding()
            HStack{
                Button(action: {inputString(input: "7")}, label: {
                    Text("7")
                        .fontWeight(.heavy)
                        .padding()
                        .frame(width: 80)
                        .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                })
                Button(action: {inputString(input: "8")}, label: {
                    Text("8")
                        .fontWeight(.heavy)
                        .padding()
                        .frame(width: 80)
                        .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                })
                Button(action: {inputString(input: "9")}, label: {
                    Text("9")
                        .fontWeight(.heavy)
                        .padding()
                        .frame(width: 80)
                        .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                })
                Button(action:{checkSymbols(symbol: "x")}, label: {
                    Text("x")
                        .fontWeight(.heavy)
                        .padding()
                        .frame(width: 100)
                        .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                        .foregroundColor(Color.black)
                    
                })
                .background(ButtonBackground())
            }
            .padding()
            HStack{
//                Text("")
//                    .padding()
//                    .frame(width: 80)
                Button(action: {inputString(input: "0")}, label: {
                    Text("0")
                        .fontWeight(.heavy)
                        .padding()
                        .frame(width: 168)
                        .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                })
                Button(action:{outputString()}, label: {
                    Text("=")
                        .fontWeight(.heavy)
                        .padding()
                        .frame(width: 80)
                        .border(Color.white, width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                        .foregroundColor(Color.white)
                        .background(Color.blue)
                })
                Button(action: {checkSymbols(symbol: "/")}, label: {
                    Text("/")
                        .fontWeight(.heavy)
                        .padding()
                        .frame(width: 100)
                        .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                        .foregroundColor(Color.black)
                    
                })
                .background(ButtonBackground())
            }
            .padding()

            HStack{
                Button(action: {clearValue()}, label: {
                    Text("clear")
                        .fontWeight(.heavy)
                        .padding()
                        .frame(width: 200,height: 60)
                        .border(Color.red, width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                        .font(.largeTitle)
                        .background(Color.red)
                        .foregroundColor(Color.white)
                })
            }
        }
        
    }
}
struct ButtonBackground: View {
    @State private var colorChange = false
    
    let timer = Timer.publish(every: 1,on: .main,in: .default).autoconnect()
    var body: some View {
        LinearGradient(gradient: Gradient(colors: [
                                            colorChange ? .gray : .white,
                                            colorChange ? .white : .gray]),
                       startPoint: .leading, endPoint: /*@START_MENU_TOKEN@*/.trailing/*@END_MENU_TOKEN@*/)
//            .onReceive(timer){
//                time in withAnimation(.easeInOut){
//                    self.colorChange.toggle()
//
//                }
//            }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .background(/*@START_MENU_TOKEN@*//*@PLACEHOLDER=View@*/Color.white/*@END_MENU_TOKEN@*/)
    }
}
