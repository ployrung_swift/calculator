//
//  Calculator_61160154App.swift
//  Calculator_61160154
//
//  Created by informatics on 2/25/21.
//

import SwiftUI

@main
struct Calculator_61160154App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
